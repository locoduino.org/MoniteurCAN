/*=======================================================================================
 * Configuration.h
 *---------------------------------------------------------------------------------------
 * Un moniteur pour gerer le bus CAN
 *
 * LOCODUINO, http://www.locoduino.org
 *
 * Auteur : Jean-Luc B\'echennec
 *
 * Ce logiciel est distribu\'e sous la licence GNU Public v2 (GPLv2)
 *
 * Please read the LICENCE file
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#ifndef __CONFIGURATION_H__
#define __CONFIGURATION_H__

#include <Arduino.h>

/*---------------------------------------------------------------------------------------
 * Fonction de lecture et d'écriture de la configuration
 */
namespace config {
  /* memorise la vitesse du bus CAN en EEPROM                   */
  void fixeVitesseCANParDefaut(byte vitesse);
  /* renvoie la vitesse du bus CAN memorisee                    */
  byte vitesseCANParDefaut();
  /* memorise l'autodemmarage du CAN dans l'EEPROM              */
  void fixeAutodemarrageCAN(bool autoDemarrage);
  /* renvoie l'autodemarrage                                    */
  bool autodemarrageCAN();
  /* memorise le fait que l'on affiche ou non les trames recues */
  void fixeAfficheTrames(bool affiche);
  /* renvoie l'affichage des trames recues                      */
  bool afficheTrames();
}

#endif

