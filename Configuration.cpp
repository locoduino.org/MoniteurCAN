/*=======================================================================================
 * Configuration.cpp
 *---------------------------------------------------------------------------------------
 * Un moniteur pour gerer le bus CAN
 *
 * LOCODUINO, http://www.locoduino.org
 *
 * Auteur : Jean-Luc B\'echennec
 *
 * Ce logiciel est distribu\'e sous la licence GNU Public v2 (GPLv2)
 *
 * Please read the LICENCE file
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#include "Configuration.h"
#include <EEPROM.h>

/*
 * Cartographie de l'EEPROM des donnees de configuration
 */
static const word VITESSE_CAN    = 0; /* Vitesse du bus CAN           */
static const word AUTO_CAN       = 1; /* autodemarrage du bus CAN     */
static const word AFFICHE_TRAMES = 2; /* affichage des trames recues  */

namespace config {
  /*
   * Memorise la vitesse du bus CAN en EEPROM
   */
  void fixeVitesseCANParDefaut(byte vitesse)
  {
    EEPROM.update(VITESSE_CAN, vitesse);
  }

  /*
   * Retourne la vitesse du bus CAN
   */
  byte vitesseCANParDefaut()
  {
    return EEPROM.read(VITESSE_CAN);
  }

  /*
   * memorise l'autodemmarage du CAN dans l'EEPROM
   */
  void fixeAutodemarrageCAN(bool autoDemarrage)
  {
    EEPROM.update(AUTO_CAN, autoDemarrage);
  }
  
  /*
   * renvoie l'autodemarrage
   */
  bool autodemarrageCAN()
  {
    return EEPROM.read(AUTO_CAN);
  }

  /*
   * memorise le fait que l'on affiche ou non les trames recues
   */
  void fixeAfficheTrames(bool affiche)
  {
    EEPROM.update(AFFICHE_TRAMES, affiche);
  }

  /*
   * renvoie l'affichage des trames recues
   */
  bool afficheTrames()
  {
    return EEPROM.read(AFFICHE_TRAMES);
  }

}

