/*=======================================================================================
 * CanRing.h
 *---------------------------------------------------------------------------------------
 * Un moniteur pour gerer le bus CAN
 *
 * LOCODUINO, http://www.locoduino.org
 *
 * Auteur : Jean-Luc B\'echennec
 *
 * Ce logiciel est distribu\'e sous la licence GNU Public v2 (GPLv2)
 *
 * Please read the LICENCE file
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#ifndef __CANRING_H__
#define __CANRING_H__

#include "CanMessage.h"

const byte CANRING_SIZE = 16;

class CanRing
{
  private:
    CanMessage mMessageBuffer[CANRING_SIZE];
    byte mReadIndex;
    byte mSize;

  public:
    CanRing()          { mReadIndex = mSize = 0; }
    bool full() const  { return (mSize == CANRING_SIZE); }
    bool empty() const { return (mSize == 0); }
    byte size() const  { return mSize; }
    bool receive();
    bool receiveAndOverride();
    bool get(CanMessage &message);
    bool printAndForget();
    bool printlnAndForget();
};

#endif

