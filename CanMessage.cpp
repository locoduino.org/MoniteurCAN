/*=======================================================================================
 * CanMessage.cpp
 *---------------------------------------------------------------------------------------
 * Un moniteur pour gerer le bus CAN
 * 
 * LOCODUINO, http://www.locoduino.org
 *
 * Auteur : Jean-Luc B\'echennec
 *
 * Ce logiciel est distribu\'e sous la licence GNU Public v2 (GPLv2)
 *
 * Please read the LICENCE file
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#include "CanMessage.h"
#include "Can.h"

/*
 * Set the data content of a CAN message, does not change the id
 */
void CanMessage::setData(byte length, const byte *buffer)
{
  if (mId != 0xFFFF) {
    length = min(length, 8);
    mLength = length;
    while (length-- > 0) mBuffer[length] = buffer[length];
  }
}

/*
 * Set the content of a CAN message
 */
void CanMessage::set(word id, byte length, const byte *buffer)
{
  if (setId(id)) {
    setData(length, buffer);
  }
}

/*
 * Get the data content of a CAN message
 */
void CanMessage::getData(byte &length, byte* const buffer)
{
  if (mId != 0xFFFF) {
    byte len = mLength;
    length = mLength;
    while (len-- > 0) buffer[len] = mBuffer[len];
  }
}

void CanMessage::get(word &id, byte &length, byte* const buffer)
{
  if (mId != 0xFFFF) {
    id = mId;
    getData(length, buffer);
  }
}

bool CanMessage::receive()
{
  bool availableMessage = (can.checkReceive() == CAN_MSGAVAIL);
  if (availableMessage) {
    unsigned long id;
    can.readMsgBufID(&id, &mLength, mBuffer);
    mId = id; /* standard frames only */
  }
  return availableMessage;
}

void CanMessage::send()
{
  can.sendMsgBuf(mId, 0, mLength, mBuffer);
}

void CanMessage::print()
{
  Serial.print('[');
  if (mId != 0xFFFF) Serial.print(mId);
  else Serial.print('*');
  Serial.print(']');
  if (mId != 0xFFFF) {
    Serial.print(F(" ("));
    Serial.print(mLength);
    Serial.print(')');
    for (byte index = 0; index < mLength; index++) {
      Serial.print('\t');
      Serial.print(mBuffer[index]);
    }
  }
}

void CanMessage::println()
{
  print();
  Serial.println();
}

CanMessage &CanMessage::operator=(CanMessage &rightMessage)
{
  mId = rightMessage.mId;
  byte len = mLength = rightMessage.mLength;
  while (len-- > 0) mBuffer[len] = rightMessage.mBuffer[len];
  return *this;
}


