/*=======================================================================================
 * Can.cpp
 *---------------------------------------------------------------------------------------
 * Un moniteur pour gerer le bus CAN
 * 
 * LOCODUINO, http://www.locoduino.org
 *
 * Auteur : Jean-Luc B\'echennec
 *
 * Ce logiciel est distribu\'e sous la licence GNU Public v2 (GPLv2)
 *
 * Please read the LICENCE file
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#include "Can.h"
#include "Configuration.h"
#include "CanRing.h"

const byte SPIChipSelectPin = 10;
const byte CANInterruptPin = 3;

MCP_CAN can(SPIChipSelectPin);

bool canMessageReceived = false;

static char buffer[10];

/*---------------------------------------------------------------------------------------
 * Vitesses CAN autorisees par la bibliotheque
 */
const char vitesse5KBPS[]    PROGMEM = "5 KB";
const char vitesse10KBPS[]   PROGMEM = "10 KB";
const char vitesse20KBPS[]   PROGMEM = "20 KB";
const char vitesse25KBPS[]   PROGMEM = "25 KB";
const char vitesse31K25BPS[] PROGMEM = "31,25 KB";
const char vitesse33KBPS[]   PROGMEM = "33 KB";
const char vitesse40KBPS[]   PROGMEM = "40 KB";
const char vitesse50KBPS[]   PROGMEM = "50 KB";
const char vitesse80KBPS[]   PROGMEM = "80 KB";
const char vitesse83K3BPS[]  PROGMEM = "83,3 KB";
const char vitesse95KBPS[]   PROGMEM = "95 KB";
const char vitesse100KBPS[]  PROGMEM = "100 KB";
const char vitesse125KBPS[]  PROGMEM = "125 KB";
const char vitesse200KBPS[]  PROGMEM = "200 KB";
const char vitesse250KBPS[]  PROGMEM = "250 KB";
const char vitesse500KBPS[]  PROGMEM = "500 KB";
const char vitesse666KBPS[]  PROGMEM = "666 KB";
const char vitesse1000KBPS[] PROGMEM = "1 MB";
const char vitessePasOk[]    PROGMEM = "erronnee";

const char* const tableVitesse[] PROGMEM = {
  vitessePasOk, vitesse5KBPS, vitesse10KBPS, vitesse20KBPS, vitesse25KBPS,
  vitesse31K25BPS, vitesse33KBPS, vitesse40KBPS, vitesse50KBPS, vitesse80KBPS,
  vitesse83K3BPS, vitesse95KBPS, vitesse100KBPS, vitesse125KBPS, vitesse200KBPS,
  vitesse250KBPS, vitesse500KBPS, vitesse666KBPS, vitesse1000KBPS
};

CanRing canRingBuffer;

/*---------------------------------------------------------------------------------------
 * Fonction de mise enroute ou d'arret de l'affichage des trames CAN recues
 */
static bool afficheTrames = false;

void fixeAfficheTrames(bool affiche)
{
  afficheTrames = affiche;
}

/*---------------------------------------------------------------------------------------
 * fonctions d'affichage de la vitesse
 */
void afficheVitesse(HardwareSerial &serial, byte vitesse, const __FlashStringHelper *msg)
{
  if (! vitesseOk(vitesse)) serial.print(msg);
  else {
    strcpy_P(buffer, (char*)pgm_read_word(&(tableVitesse[vitesse])));
    serial.print(buffer);
  }
}

void afficheVitesseln(
  HardwareSerial &serial,
  byte vitesse,
  const __FlashStringHelper *msg)
{
  afficheVitesse(serial, vitesse, msg);
  serial.println();
}

/*---------------------------------------------------------------------------------------
 * vérifie la vitesse du bus CAN
 */
bool vitesseOk(byte vitesse)
{
  return (vitesse > 0 && vitesse <= CAN_1000KBPS);
}

/*---------------------------------------------------------------------------------------
 * Demarre le bus CAN
 */
bool demarreCAN(const byte vitesse, byte reessai)
{
  while (reessai-- > 0) {
    if (can.begin(vitesse) == CAN_OK) break;
    delay(500);
  }
  return (reessai != 0);
}

/*---------------------------------------------------------------------------------------
 * Autodemarre le bus CAN
 */
bool autodemarreCAN()
{
  if (config::autodemarrageCAN()) {
    byte vitesse = config::vitesseCANParDefaut();
    if (! vitesseOk(vitesse)) {
      Serial.println(F("Le CAN est en autodemarrage mais la vitesse"
                       "par defaut n'est pas connue"));
    }
    else {
      Serial.print(F("Demarrage du CAN..."));
      if (demarreCAN(vitesse)) {
        Serial.println(F(" Ok"));
        Serial.print(F("CAN demarre a "));
        afficheVitesseln(Serial,vitesse,F("vitesse invalide"));
      }
    }
  }
}

/*---------------------------------------------------------------------------------------
 * Interruption CAN et intitialisation de l'interruption CAN
 */
void canISR()
{
  canMessageReceived = true;
}
 
void initialisationCAN()
{
  fixeAfficheTrames(config::afficheTrames());
  pinMode(CANInterruptPin, INPUT_PULLUP);
  attachInterrupt(digitalPinToInterrupt(CANInterruptPin), canISR, FALLING);
}

/*---------------------------------------------------------------------------------------
 * poll le CAN pour récupérer les messages
 */
void pollCAN()
{
  if (canMessageReceived) {
    canMessageReceived = false;
    while(canRingBuffer.receiveAndOverride());
  }
  if (afficheTrames) canRingBuffer.printlnAndForget();
}

