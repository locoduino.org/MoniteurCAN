/*=======================================================================================
 * CanRing.cpp
 *---------------------------------------------------------------------------------------
 * Un moniteur pour gerer le bus CAN
 *
 * LOCODUINO, http://www.locoduino.org
 *
 * Auteur : Jean-Luc B\'echennec
 *
 * Ce logiciel est distribu\'e sous la licence GNU Public v2 (GPLv2)
 *
 * Please read the LICENCE file
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#include "CanRing.h"

bool CanRing::receive()
{
  bool doIt = ! full();
  if (doIt) {
    byte writeIndex = (mReadIndex + mSize) % CANRING_SIZE;
    if (mMessageBuffer[writeIndex].receive()) {
      mSize++;
    }
    else doIt = false;
  }
  return doIt;
}

bool CanRing::receiveAndOverride()
{
  bool doIt = false;

  if (full()) {
    if (mMessageBuffer[mReadIndex].receive()) {
      mReadIndex = (mReadIndex + 1) % CANRING_SIZE;
      doIt = true;
    }
  }
  else {
    byte writeIndex = (mReadIndex + mSize) % CANRING_SIZE;
    if (mMessageBuffer[writeIndex].receive()) {
      mSize++;
      doIt = true;
    }
  }
  return doIt;
}

bool CanRing::get(CanMessage &message)
{
  bool result = ! empty();
  if (result) {
    message = mMessageBuffer[mReadIndex];
    mReadIndex = (mReadIndex + 1) % CANRING_SIZE;
    mSize--;
  }
  return result;
}

bool CanRing::printAndForget()
{
  bool result = ! empty();
  if (result) {
    mMessageBuffer[mReadIndex].print();
    mReadIndex = (mReadIndex + 1) % CANRING_SIZE;
    mSize--;
  }
  return result;  
}

bool CanRing::printlnAndForget()
{
  bool result = printAndForget();
  if (result) Serial.println();
  return result;
}

