/*=======================================================================================
 * Can.h
 *---------------------------------------------------------------------------------------
 * Un moniteur pour gerer le bus CAN
 * 
 * LOCODUINO, http://www.locoduino.org
 *
 * Auteur : Jean-Luc B\'echennec
 *
 * Ce logiciel est distribu\'e sous la licence GNU Public v2 (GPLv2)
 *
 * Please read the LICENCE file
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#ifndef __CAN_H__
#define __CAN_H__

#include "mcp_can.h"

extern MCP_CAN can;

/*---------------------------------------------------------------------------------------
 * Affiche la vitesse du bus CAN
 */
void afficheVitesse(HardwareSerial &serial, byte vitesse, const __FlashStringHelper *msg);

void afficheVitesseln(HardwareSerial &serial, byte vitesse, const __FlashStringHelper *msg);

/*---------------------------------------------------------------------------------------
 * Fonction de mise enroute ou d'arret de l'affichage des trames CAN recues
 */
void fixeAfficheTrames(bool affiche);

/*---------------------------------------------------------------------------------------
 * vérifie la vitesse du bus CAN
 */
bool vitesseOk(byte vitesse);

/*---------------------------------------------------------------------------------------
 * Demarre le bus CAN
 */
bool demarreCAN(byte vitesse, byte reessai = 4);

/*---------------------------------------------------------------------------------------
 * Autodemarre le bus CAN
 */
bool autodemarreCAN();

/*---------------------------------------------------------------------------------------
 * Intitialisation de l'interruption CAN
 */
void initialisationCAN();

/*---------------------------------------------------------------------------------------
 * recupere les message CAN
 */
void pollCAN();

#endif

