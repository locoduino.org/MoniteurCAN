/*=======================================================================================
 * CanMessage.h
 *---------------------------------------------------------------------------------------
 * Un moniteur pour gerer le bus CAN
 * 
 * LOCODUINO, http://www.locoduino.org
 *
 * Auteur : Jean-Luc B\'echennec
 *
 * Ce logiciel est distribu\'e sous la licence GNU Public v2 (GPLv2)
 *
 * Please read the LICENCE file
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#ifndef __CANMESSAGE_H__
#define __CANMESSAGE_H__

#include <Arduino.h>

class CanMessage
{
  private:
    word mId;
    byte mLength;
    byte mBuffer[8];

  public:
    CanMessage() { mId = 0xFFFF; } /* message vide, id = 0xFFFF */
    bool setId(word id)
    {
      if (id < (1 << 11)) {
        mId = id;
        return true;
      }
      else {
        return false;
      }
    }
    void unsetId() { mId = 0xFFFF; }
    bool id(word &id)   { if (mId != 0xFFFF) id = mId; return (mId != 0xFFFF); }
    void setData(byte length, const byte *buffer);
    void getData(byte &length, byte* const buffer);
    void set(const word id, byte length, const byte *buffer);
    void get(word &id, byte &length, byte* const buffer);
    bool receive();
    void send();
    void print();
    void println();
    CanMessage &operator=(CanMessage &rightMessage);
};

#endif
