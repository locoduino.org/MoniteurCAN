/*=======================================================================================
 * Moniteur.cpp
 *---------------------------------------------------------------------------------------
 * Un moniteur pour gerer le bus CAN
 *
 * LOCODUINO, http://www.locoduino.org
 *
 * Auteur : Jean-Luc B\'echennec
 *
 * Ce logiciel est distribu\'e sous la licence GNU Public v2 (GPLv2)
 *
 * Please read the LICENCE file
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#include "Moniteur.h"
#include "CommandInterpreter.h"
#include "HardwareSerial.h"
#include "Configuration.h"
#include "mcp_can_dfs.h"
#include "Can.h"
#include "CanMessage.h"

/*---------------------------------------------------------------------------------------
 * Version du logiciel
 */
const byte versionMajeure = 1;
const byte versionMineure = 0;
const byte revision       = 0;

/*---------------------------------------------------------------------------------------
 * Arguments oui/non en flash
 */
const char ouiArgument[] PROGMEM = "oui";
const char nonArgument[] PROGMEM = "non";

/*---------------------------------------------------------------------------------------
 * Moniteur de commandes
 */
CommandInterpreter interpreter(Serial);

/*---------------------------------------------------------------------------------------
 * fonctions de conversion d'une vitesse en flottant en code de vitesse
 */
byte codeVitessePourVitesse(const float vitesse)
{
  if (vitesse == 5.0)         return CAN_5KBPS;
  else if (vitesse == 10.0)   return CAN_10KBPS;
  else if (vitesse == 20.0)   return CAN_20KBPS;
  else if (vitesse == 25.0)   return CAN_25KBPS;
  else if (vitesse == 31.25)  return CAN_31K25BPS;
  else if (vitesse == 33.0)   return CAN_33KBPS;
  else if (vitesse == 40.0)   return CAN_40KBPS;
  else if (vitesse == 50.0)   return CAN_50KBPS;
  else if (vitesse == 80.0)   return CAN_80KBPS;
  else if (vitesse == 83.3)   return CAN_83K3BPS;
  else if (vitesse == 95.0)   return CAN_95KBPS;
  else if (vitesse == 100.0)  return CAN_100KBPS;
  else if (vitesse == 125.0)  return CAN_125KBPS;
  else if (vitesse == 200.0)  return CAN_200KBPS;
  else if (vitesse == 250.0)  return CAN_250KBPS;
  else if (vitesse == 500.0)  return CAN_500KBPS;
  else if (vitesse == 666.0)  return CAN_666KBPS;
  else if (vitesse == 1000.0) return CAN_1000KBPS;
  else return 0;
}

/*---------------------------------------------------------------------------------------
 * Fonction d'erreur du nombre d'arguments
 */
static void nombreDArgumentsIncorrect(HardwareSerial &serial, byte minimum, byte maximum)
{
  if (minimum == maximum) {
    if (minimum == 0) serial.println(F("Erreur. Commande sans argument"));
    else {
      serial.print(F("Erreur. "));
      serial.print(minimum);
      serial.println(F(" arguments attendus"));
    }
  }
  else {
    serial.print(F("Erreur. Nombre d'arguments incorrect, "));
    serial.print(F("entre "));
    serial.print(minimum);
    serial.print(F(" et "));
    serial.print(maximum);
    serial.println(F(" attendus"));
  }
}

/*---------------------------------------------------------------------------------------
 * Teste l'argument et renvoie true (oui) ou false (non) 
 */
bool ouiNon(char *arg, bool &result)
{
  bool erreur = false;
  if (strcmp_P(arg, ouiArgument) == 0) result = true;
  else if (strcmp_P(arg, nonArgument) == 0) result = false;
  else erreur = true;
  return erreur;
}
 
/*---------------------------------------------------------------------------------------
 * Commande d'affichage de la version du logiciel
 */
void afficheVersion(CommandInterpreter &cmdInt, byte nbArgs)
{
  if (nbArgs == 0) {
    cmdInt.Serial().print(versionMajeure);
    cmdInt.Serial().print('.');
    cmdInt.Serial().print(versionMineure);
    cmdInt.Serial().print('.');
    cmdInt.Serial().println(revision);
  }
  else cmdInt.Serial().println(F("Cette commande ne prend pas d'argument"));
}

Command afficheVersionCmd("ver", afficheVersion, "Affiche la version du logiciel");

/*---------------------------------------------------------------------------------------
 * Commande de manipulation de la vitesse du CAN
 */
void vitesseCAN(CommandInterpreter &cmdInt, byte nbArgs)
{
  if (nbArgs == 0) { /* consultation de la vitesse */
    byte vitesse = config::vitesseCANParDefaut();
    afficheVitesseln(cmdInt.Serial(), vitesse, F("Pas de vitesse configuree"));
  }
  else if (nbArgs == 1) { /* fixe une nouvelle vitesse */
    float vitesse;
    byte codeVitesse;
    bool erreur = false;
    if (cmdInt.readFloat(vitesse)) {
      erreur = ((codeVitesse = codeVitessePourVitesse(vitesse)) == 0);
    }
    else erreur = true;
    if (erreur) cmdInt.Serial().println(F("Vitesse incorrecte. Valeurs autorisee :\n"
      "    5, 10, 20, 25, 31.25, 33, 40, 50, 80, 83.3,\n"
      "    95, 100, 125, 200, 250, 500, 666 et 1000"));
    else config::fixeVitesseCANParDefaut(codeVitesse);
  }
  else nombreDArgumentsIncorrect(cmdInt.Serial(), 0, 1);
}

Command vitesseCmd("vit", vitesseCAN, "Affiche ou change la vitesse du bus CAN");

/*---------------------------------------------------------------------------------------
 * Commande de demarrage du bus CAN a la vitesse specifiee
 */
void demarreCAN(CommandInterpreter &cmdInt, byte nbArgs)
{
  byte vitesse = 0;
  if (nbArgs == 0) {
    /* Si le nombre d'arguments est 0, recupere la vitesse de l'EEPROM */
    byte vitesseEEPROM = config::vitesseCANParDefaut();
    if (vitesseEEPROM >= CAN_5KBPS && vitesseEEPROM <= CAN_1000KBPS) {
      /* Vitesse ok dans l'EEPROM */
      vitesse = vitesseEEPROM;
    }
  }
  else if (nbArgs == 1) {
    /* Si le nombre d'arguments est 1, la vitesse est l'argument */
    float argumentVitesse;
    byte codeVitesse;
    bool erreur = false;
    if (cmdInt.readFloat(argumentVitesse)) {
      erreur = ((codeVitesse = codeVitessePourVitesse(argumentVitesse)) == 0);
    }
    else erreur = true;
    if (erreur) cmdInt.Serial().println(F("Vitesse incorrecte. Valeurs autorisee :\n"
      "    5, 10, 20, 25, 31.25, 33, 40, 50, 80, 83.3,\n"
      "    95, 100, 125, 200, 250, 500, 666 et 1000"));
    else vitesse = codeVitesse;
  }
  else nombreDArgumentsIncorrect(cmdInt.Serial(), 0, 1);

  if (vitesse != 0) {
    if (demarreCAN(vitesse)) {
      cmdInt.Serial().print(F("CAN demarre a "));
      afficheVitesseln(cmdInt.Serial(), vitesse, F("??"));
    }
    else {
      /* Impossible de communiquer avec le MCP2515 */
      cmdInt.Serial().println(F("Le controleur CAN de repond pas"));
    }
  }
}

Command demarreCmd("dem", demarreCAN, "Demarre le CAN");

/*---------------------------------------------------------------------------------------
 * Parametre l'autodemarrage du CAN
 */
void autoCAN(CommandInterpreter &cmdInt, byte nbArgs)
{
  bool erreur = false;
  if (nbArgs == 0) {
    if (config::autodemarrageCAN()) cmdInt.Serial().println(F("oui"));
    else cmdInt.Serial().println(F("non"));
  }
  else if (nbArgs == 1) {
    char *argument;
    if (cmdInt.readString(argument)) {
      bool valeur;
      erreur = ouiNon(argument, valeur);
      if (! erreur) config::fixeAutodemarrageCAN(valeur);
    }
    if (! vitesseOk(config::vitesseCANParDefaut())) {
      cmdInt.Serial().println(F("Attention, le CAN est en autodemmarrage mais la vitesse n'est pas connue"));
    }
  }
  else {
    nombreDArgumentsIncorrect(cmdInt.Serial(), 0, 1);
    erreur = true;
  }
  if (erreur) cmdInt.Serial().println(F("Usage: auto <oui|non>"));
}

Command autoCmd("auto", autoCAN, "Determine l'autodemarrage du CAN");

/*---------------------------------------------------------------------------------------
 * Demarre et arrete l'affichage des trames recues
 */
void afficheTrames(CommandInterpreter &cmdInt, byte nbArgs)
{
  bool erreur = false;
  if (nbArgs == 0) {
    if (config::afficheTrames()) cmdInt.Serial().println(F("oui"));
    else cmdInt.Serial().println(F("non"));
  }
  else if (nbArgs == 1) {
    char *argument;
    if (cmdInt.readString(argument)) {
      bool valeur;
      erreur = ouiNon(argument, valeur);
      if (! erreur) {
        fixeAfficheTrames(valeur);
        config::fixeAfficheTrames(valeur);
      }
    }
  }
  else {
    nombreDArgumentsIncorrect(cmdInt.Serial(), 0, 1);
    erreur = true;
  }
  if (erreur) cmdInt.Serial().println(F("Usage: aff <oui|non>"));
}

Command afficheCmd("aff", afficheTrames, "Affiche ou non les trames recues");

/*---------------------------------------------------------------------------------------
 * envoie une trame CAN
 */
void envoieTrame(CommandInterpreter &cmdInt, byte nbArgs)
{
  bool erreur = false;
  if (nbArgs > 0 && nbArgs <= 9) {
    word id;
    if (cmdInt.readUnsignedInt(id)) {
      byte buffer[8];
      byte length = 0;
      nbArgs--;
      for (byte i = 0; i < nbArgs; i++) {
        if (cmdInt.readByte(buffer[i])) length++;
        else {
          erreur = true;
          break;
        }
      }
      if (! erreur) {
        CanMessage message;
        message.set(id, length, buffer);
        message.send();
      }
    }
    else erreur = true;
  }
  else {
    nombreDArgumentsIncorrect(cmdInt.Serial(), 1, 9);
    erreur = true;
  }
  if (erreur) cmdInt.Serial().println(F("Usage: env <id> [<d0> ...]"));
}

Command envoieTrameCmd("env", envoieTrame, "Envoie un message CAN");

/*---------------------------------------------------------------------------------------
 * Demarre le moniteur lorsqu'on est en mode configuration
 */
void demarreMoniteur()
{
  Serial.begin(115200);
  Serial.print('\f');
  Serial.println(F("*--------------------------*"));
  Serial.println(F("*  Moniteur CAN LOCODUINO  *"));
  Serial.println(F("*     Licence GPL v2.0     *"));
  Serial.println(F("* http://www.locoduino.org *"));
  Serial.println(F("*--------------------------*"));
  interpreter.setPrompt(F("CAN>"));
  interpreter.addCommand(afficheVersionCmd);
  interpreter.addCommand(vitesseCmd);
  interpreter.addCommand(demarreCmd);
  interpreter.addCommand(autoCmd);
  interpreter.addCommand(afficheCmd);
  interpreter.addCommand(envoieTrameCmd);
}

/*---------------------------------------------------------------------------------------
 * Assure la communication du moniteur. A appeler dans loop
 */
void moniteur()
{
  CommandInterpreter::update();
}



