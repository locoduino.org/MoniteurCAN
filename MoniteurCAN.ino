/*=======================================================================================
 * MoniteurCAN
 *---------------------------------------------------------------------------------------
 * Un moniteur pour gerer le bus CAN
 * 
 * LOCODUINO, http://www.locoduino.org
 *
 * Auteur : Jean-Luc B\'echennec
 *
 * Ce logiciel est distribu\'e sous la licence GNU Public v2 (GPLv2)
 *
 * Please read the LICENCE file
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#include <SPI.h>
#include "Can.h"

#include "Moniteur.h"

void setup() {
  demarreMoniteur();
  initialisationCAN();
  autodemarreCAN();
}

void loop() {
  moniteur();
  pollCAN();
}
